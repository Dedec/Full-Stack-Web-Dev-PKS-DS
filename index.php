<?php

trait Hewan
{
    public $nama;
    public $darah = "50";
    public $jumlahKaki;
    public $keahlian;

    public function atraksi()
    {
        echo $this->nama . ", sedang " . $this->keahlian . "<br>";
    }
}

trait Fight
{
    public $attackPower;
    public $defencePower;

    public function serang()
    {
        echo $this->nama . ", sedang menyerang" . $this->nama;
    }

    public function diserang()
    {
        echo $this->nama . ", sedang diserang" . $this->nama;
    }
}

class Elang
{
    use Hewan;

    public function __construct($nama, $darah, $jumlahKaki, $keahlian)
    {
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlah_kaki = $jumlahKaki;
        $this->keahlian = $keahlian;
    }

    public function getInfoHewan()
    {
    }
}


class Harimau
{
    use Hewan;

    public function __construct($nama, $darah, $jumlahKaki, $keahlian, $attackPower, $defencePower)
    {
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlah_kaki = $jumlahKaki;
        $this->keahlian = $keahlian;
        $this->attack_power = $attackPower;
        $this->defence_power = $defencePower;
    }

    public function getInfoHewan()
    {
    }
}

$elang1 = new Elang("Elang", 50, 2, "terbang tinggi");
$elang1->atraksi();

$harimau1 = new Elang("Harimau", 50, 2, "lari cepat");
$harimau1->atraksi();

// $harimau1 = new Harimau("Harimau", 50, 4, "lari cepat", 7, 8);
// $harimau1->serang();
// $harimau1->diserang();
