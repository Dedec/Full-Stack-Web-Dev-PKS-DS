// Jawaban Soal 1
console.log('Jawaban soal 1 \n');
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];


for (i = 0; i < daftarHewan.length; i++) {
    daftarHewan.sort();
    console.log(daftarHewan[i]);
  } 

  console.log('\n');
//Jawaban Soal 2
console.log('Jawaban soal 2 \n');

var Data = {
    name: 'John',
    age: 30,
    adress: 'Jalan Pelesiran',
    hobby: 'Gaming',
    introduce: function(){
        return 'Nama saya: ' + this.name +('\n')+
               'Umur saya: ' + this.age + ' tahun' + ('\n')+
               'Alamat saya di: ' + this.adress + ('\n')+
               'dan saya punya hobi: ' + this.hobby +('\n');
    }
}

console.log(Data.introduce());

console.log('\n');
//Jawaban Soal 3
console.log('Jawaban soal 3 \n');

function hitung_huruf_vokal(hasil){
    var namaSatu = "Muhammad";
    var namaDua = "Iqbal";

    var hurufVokal_1 = namaSatu.match(/[aeiou]/ig).join("");
    var hurufVokal_2 = namaDua.match(/[aeiou]/ig).join("");
    
    var hasil = hurufVokal_1.length + hurufVokal_2.length;
    return 'Jumlah total huruf vokal yang ada di Variabel namaSatu dan namaDua adalah ' + hasil;
}

console.log(hitung_huruf_vokal());

console.log('\n');
//Jawaban Soal 4_belum selesai
console.log('Jawaban soal 4 \n');

function hitung(nilai){
    var x = 2
    var hasil = nilai - x;

    return hasil;
    }

    console.log( hitung(0) ) // -2
    console.log( hitung(1) ) // 0
    console.log( hitung(2) ) // 2
    console.log( hitung(3) ) // 4
    console.log( hitung(5) ) // 8
