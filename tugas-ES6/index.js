console.log('Jawaban soal 1');
//Jawaban LuasPersegiPanjang
const luasPersegiPanjang = (panjang, lebar) =>{ return 'Luas Persegi Panjangnya adalah: ' + panjang * lebar;}
console.log(luasPersegiPanjang(2,3));

//Jawaban KelilingPersegiPanjang
const kelilingPersegiPanjang = (panjang, lebar) =>{
    let hasil = 2*(panjang+lebar);
    return 'Keliling Persegi Panjangnya adalah: ' + hasil;
}
console.log(kelilingPersegiPanjang(2,3));

console.log('\n');
console.log('Jawaban soal 2 pakai Arrow Function');
//Jawaban menggunakan Arrow Function
const fullName = (firstName, lastName) =>{ return 'Nama lengkapnya adalah: ' + firstName + ' ' + lastName;}
console.log((fullName('William', 'Imoh')));

console.log('\n');
console.log('Jawaban soal 2 pakai Object');
//Jawaban menggunakan Object
const Data_Orang ={
    firstName : 'William',
    lastName : 'Imoh',
    fullName: function(){
        return 'Nama lengkapnya adalah: ' + this.firstName + ' ' + this.lastName;
    }
}
console.log(Data_Orang.fullName());

console.log('\n');
console.log('Jawaban soal 3');
//Jawaban
console.log(({firstName, lastName, address, hobby} = {firstName: 'Muhammad', lastName: 'Iqbal Mubaroq', adress: 'Jalan Ranamanyar', hobby: 'playing football'}));

console.log('\n');
console.log('Jawaban soal 4');
//Jawaban
let west = ["Will", "Chris", "Sam", "Holly"]
let east = ["Gill", "Brian", "Noel", "Maggie"]

let combined = [...west, ...east]
console.log(combined)

console.log('\n');
console.log('Jawaban soal 5');
//Jawaban
const planet = "earth" ;
const view = "glass" ;
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet} `

console.log(before);


